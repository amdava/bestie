# frozen_string_literal: true

Rails.application.config.session_store :cookie_store, expire_after: 365.days, httponly: true, same_site: :strict, secure: Rails.env.production?
