# frozen_string_literal: true

module MailMonitor
  def self.delivering_email(message)
    if ENV["EMAIL_MONITOR_DEST"].present?
      message.bcc = [] if message.bcc.blank?
      message.bcc << ENV["EMAIL_MONITOR_DEST"]
    end
  end
end

if Rails.env.production?
  ActionMailer::Base.register_interceptor(MailMonitor)
end
