# frozen_string_literal: true

# We are using Delayed::Job's method of retrying jobs because if there's a fault, once it's corrected, a single SQL update can put all the jobs in the path to be run again.
# https://stackoverflow.com/questions/66305695/how-do-i-make-active-job-retry-all-jobs-forever
if Rails.application.config.active_job.queue_adapter == :delayed_job
  Delayed::Worker.destroy_failed_jobs = false
  # Delayed::Worker.max_attempts = Float::INFINITY
else
  raise "Unexpected queue_adapter: #{Rails.application.config.active_job.queue_adapter}, make sure to configure to not lose jobs: https://stackoverflow.com/questions/66305695/how-do-i-make-active-job-retry-all-jobs-forever"
end
