if ENV["SENTRY_DSN"].blank?
  if Rails.env.production?
    # raise "SENTRY_DSN needs to be defined"
  end
else
  Sentry.init do |config|
    config.dsn = ENV["SENTRY_DSN"]
    config.breadcrumbs_logger = [:active_support_logger]

    # To activate performance monitoring, set one of these options.
    # We recommend adjusting the value in production:
    config.traces_sample_rate = 0.5
    # or
    config.traces_sampler = lambda do |context|
      true
    end

    # https://docs.sentry.io/platforms/ruby/configuration/releases/
    if ENV["HEROKU_RELEASE_VERSION"].present?
      config.release = "bestie@#{ENV["HEROKU_APP_NAME"]}@#{ENV["HEROKU_RELEASE_VERSION"]}"
      config.environment = ENV["HEROKU_APP_NAME"].split("-").last
    end
  end
end
