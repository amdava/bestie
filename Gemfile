# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.0.1"

gem "activerecord-session_store", "~> 2.0.0"
gem "active_link_to", "~> 1.0.5"
gem "aws-sdk-s3", "~> 1.93.0", require: false
gem "bootsnap", "~> 1.7.2", require: false # Reduces boot times through caching; required in config/boot.rb
gem "browser", "~> 5"
gem "delayed_job_active_record", "~> 4.1.5"
gem "devise", "~> 4.7.3"
gem "devise-async", "~> 1.0.0"
gem "factory_bot_rails", "~> 6" # To be able to generate sample data in staging (which is production).
gem "faker", "~> 2" # To be able to generate sample data in staging (which is production).
gem "image_processing", "~> 1.12.1"
gem "mixpanel-ruby", "~> 2"
gem "pg", "~> 1.1"
gem "puma", "~> 5.0"
gem "rails", "~> 6.1.2", ">= 6.1.2.1"
gem "rails_admin", "~> 2.0"
gem "sentry-ruby", "~> 4"
gem "sentry-rails", "~> 4"
gem "tailwindcss-rails", "~> 0.3.3"
gem "timecop", "~> 0.9.4"
gem "webpacker", "~> 5.0"

group :development do
  gem "annotate", "~> 3.1.1"
  gem "dotenv-rails", "~> 2.7.5"
  gem "listen", "~> 3.3" # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem "rack-mini-profiler", "~> 2.0" # Display performance information such as SQL time and flame graphs for each request in your browser. Can be configured to work on production as well see: https://github.com/MiniProfiler/rack-mini-profiler/blob/master/README.md
  gem "spring", "~> 2.1.1"
  gem "web-console", "~> 4.1.0" # Access an interactive console on exception pages or by calling "console" anywhere in the code.
end

group :test do
  gem "simplecov", "~> 0.21.2", require: false
end

group :development, :test do
  gem "byebug", "~> 11.1.3", platforms: [:mri, :mingw, :x64_mingw] # Call "byebug" anywhere in the code to stop execution and get a debugger console
  gem "rspec-rails", "~> 5"
  gem "standard", "~> 1.0.0"
  gem "tzinfo-data", platforms: [:mingw, :mswin, :x64_mingw, :jruby] # Windows does not include zoneinfo files, so bundle the tzinfo-data gem
  gem "wdm", "~> 0.1.0" if Gem.win_platform?
end
