class Tracker
  def initialize
    @tracker = Mixpanel::Tracker.new(ENV["MIX_PANEL_TOKEN"]) if ENV["MIX_PANEL_TOKEN"]
  end

  def self.track(id, name, data = {})
    tracker = Tracker.new
    tracker.track(id, name, data)
  end

  def track(id, name, data = {})
    @tracker.track(id, name, data) if @tracker.present?
  end

  def alias(old_id, new_id)
    @tracker.alias(old_id, new_id) if @tracker.present?
  end

  def set_user_data(id, data)
    @tracker.people.set(id, data) if @tracker.present?
  end
end
