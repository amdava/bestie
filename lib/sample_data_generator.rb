# frozen_string_literal: true

class SampleDataGenerator
  include FactoryBot::Syntax::Methods

  def initialize
    if !(Rails.env.development? || Rails.env.test? || ENV["ALLOW_SEEDING"] == "true")
      raise "Are you out of your vulcan mind?"
    end

    $verbose = if ENV.include? "verbose" # If the verbose env is defined.
      ENV["verbose"] == "true" # and it's not true, it turns off verbosity.
    else
      true
    end
  end

  def generate
    $stdout.sync = $verbose

    delete_data

    puts "\n" if $verbose

    create_admin_in_development

    # George Clooney and Julia Roberts are the focus users, they are also both fresh users.
    george_clooney = create(:user, :with_avatar, :with_all_friendships, name: "George Clooney", email: "george.clooney@example.com")
    puts("Created user with email \"george.clooney@example.com\" with password \"TeZlk27HIp403YwthqzrRTrs6\".") if $verbose
    julia_roberts = create(:user, :with_avatar, :with_all_friendships, name: "Julia Roberts", email: "julia.roberts@example.com")
    puts("Created user with email \"julia.roberts@example.com\" with password \"TeZlk27HIp403YwthqzrRTrs6\".") if $verbose
    create(:friendship, user: george_clooney, friend: julia_roberts)

    puts "Creating lots of invitations and friendships: ([A]ccepted, [R]ejected, [P]ending)" if $verbose
    [george_clooney, julia_roberts].each do |user|
      print "  Creating invitations and friendships from #{user.name}..." if $verbose
      15.times { create_random_invitation_and_friendships_from(user) }
      puts " done." if $verbose

      print "  Creating invitations and friendships to #{user.name}..." if $verbose
      9.times { create_random_invitation_and_friendships_to(user) }
      puts " done." if $verbose
    end
    puts "Done creating lots of invitations and friendships." if $verbose
  end

  def create_user(**kwargs)
    user = create(:user, **kwargs)
    puts("  Created user #{kwargs[:name]} #{kwargs[:email]} with password \"password\".") if $verbose
    user
  end

  def delete_data
    puts "Deleting data..." if $verbose

    table_names = [
      ActiveStorage::Attachment, ActiveStorage::VariantRecord, ActiveStorage::Blob,
      ActiveRecord::SessionStore::Session,
      Delayed::Backend::ActiveRecord::Job,
      Event, Friendship, Invitation, User
    ].map(&:table_name)

    print "  Deleting attachments..." if $verbose
    ActiveStorage::Attachment.find_each do |attachment|
      attachment.purge
      print "." if $verbose
    end
    puts " done" if $verbose

    print "  Truncating tables: #{table_names.join(", ")}..." if $verbose
    User.connection.truncate_tables(*table_names)
    puts " done." if $verbose
    puts "Done deleting data." if $verbose
  end

  def create_admin_in_development
    if Rails.env.development?
      admin_email = "admin@example.com"
      admin_password = "TeZlk27HIp403YwthqzrRTrs6"
      unless Admin.where(email: admin_email).exists?
        admin = Admin.new(email: admin_email, password: admin_password)
        admin.skip_confirmation!
        admin.save!
        puts("Created admin with email \"#{admin_email}\" and password \"#{admin_password}\".") if $verbose
      end
    end
  end

  def create_random_invitation_and_friendships_from(user)
    invitation = create(:invitation, requester: user)
    print "." if $verbose
    case Random.rand(3)
    when 0
      create(:user, email: invitation.email, name: invitation.name)
      invitation.accept!
      print "a" if $verbose
    when 1
      invitation.reject!
      print "r" if $verbose
    else
      print "p" if $verbose
    end
  end

  def create_random_invitation_and_friendships_to(user)
    invitation = create(:invitation, name: user.name, email: user.email)
    print "." if $verbose
    case Random.rand(3)
    when 0
      invitation.accept!
      print "a" if $verbose
    when 1
      invitation.reject!
      print "r" if $verbose
    else
      print "p" if $verbose
    end
  end
end
