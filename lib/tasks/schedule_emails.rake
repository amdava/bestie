# frozen_string_literal: true

desc "Schedule prompts for all users that need them"
task schedule_emails: :environment do
  SendPromptsJob.perform_later
  Rails.logger.info "Scheduled sending prompts."
  SendNotOkAlertsJob.perform_later
  Rails.logger.info "Scheduled sending not-ok alerts."
end
