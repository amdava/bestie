# frozen_string_literal: true

class SendPromptsJob < ApplicationJob
  queue_as :default

  def perform(*_args)
    Rails.logger.info("Starting to send prompts to all users that need them.")
    User.find_each do |user|
      if user.needs_prompt?
        Rails.logger.info "  Sending prompt to #{user}."
        UserMailer.with(user: user).prompt.deliver_later
      else
        Rails.logger.info "  Not sending prompt to #{user} because they don't need one yet."
      end
    end
    Rails.logger.info("Done sending prompts to all users that need them.")
  end
end
