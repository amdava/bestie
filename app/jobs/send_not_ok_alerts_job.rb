# frozen_string_literal: true

class SendNotOkAlertsJob < ApplicationJob
  queue_as :default

  def perform(*_args)
    Rails.logger.info("Starting to send not-ok alerts to all users that need them.")

    User.find_each do |user|
      if user.needs_alerting_about?
        Tracker.track(user.id, "Raised Not OK Alert")
        Rails.logger.info "  #{user} needs alerting about, sending alerts to their friends."
        user.friends.find_each do |friend|
          Rails.logger.info "    Sending not ok alert to #{friend} about #{user}"
          UserMailer.with(user: friend, not_ok_user: user).not_ok_alert.deliver_later
        end
      else
        Rails.logger.info "  #{user} doesn't need alerting about. "
      end
    end
    Rails.logger.info("Done sending not-ok alerts to all users that need them.")
  end
end
