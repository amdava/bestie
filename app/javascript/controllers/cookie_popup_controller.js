import {Controller} from "stimulus"
import Cookies from "js-cookie"

export default class extends Controller {
  connect() {
    if (Cookies.get("accept-cookies")) {
      this.element.classList.add("hidden")
    }
  }

  acceptCookies() {
    console.log("accepting the cookies")
    Cookies.set("accept-cookies", true)
    this.element.classList.add("hidden")
  }
}
