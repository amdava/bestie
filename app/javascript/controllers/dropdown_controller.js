import {Controller} from "stimulus"

export default class extends Controller {
  static targets = ["menu", "button", "openButton", "closeButton"]
  static values = {open: Boolean}

  initialize() {
    this.openValue = false
  }

  connect() {
    document.addEventListener("click", this.handleClickOutside.bind(this))
  }

  disconnect() {
    document.removeEventListener("click", this.handleClickOutside.bind(this))
  }

  handleClickOutside(event) {
    this.openValue = false
  }

  toogle(even) {
    event.stopImmediatePropagation()
    this.openValue = !this.openValue
  }

  openValueChanged() {
    this.show()
  }

  show() {
    // Button state.
    this.buttonTarget.setAttribute("aria-open", this.openValue)
    this.closeButtonTarget.classList.toggle("hidden", !this.openValue)
    this.openButtonTarget.classList.toggle("hidden", this.openValue)

    // Actual menu.
    this.menuTarget.classList.toggle("hidden", !this.openValue)
  }
}
