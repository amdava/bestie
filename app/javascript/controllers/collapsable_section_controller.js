import {Controller} from "stimulus"

export default class extends Controller {
  static targets = ["body", "button", "collapseButton", "expandButton"]
  static values = {expanded: Boolean}

  toogle() {
    this.expandedValue = !this.expandedValue
  }

  expandedValueChanged() {
    this.show()
  }

  show() {
    // Button.
    this.buttonTarget.setAttribute("aria-expanded", this.expandedValue)
    this.collapseButtonTarget.classList.toggle("hidden", !this.expandedValue)
    this.expandButtonTarget.classList.toggle("hidden", this.expandedValue)
    
    // Body.
    this.bodyTarget.classList.toggle("hidden", !this.expandedValue)
  }
}
