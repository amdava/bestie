xml.instruct!
xml.urlset xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9" do
  xml.url do
    xml.loc root_url
    xml.lastmod ENV["HEROKU_RELEASE_CREATED_AT"]
    xml.changefreq "daily"
    xml.priority 1
  end
  xml.url do
    xml.loc faq_url
    xml.lastmod ENV["HEROKU_RELEASE_CREATED_AT"]
    xml.changefreq "daily"
    xml.priority 0.5
  end
  xml.url do
    xml.loc cookie_policy_url
    xml.lastmod ENV["HEROKU_RELEASE_CREATED_AT"]
    xml.changefreq "monthly"
    xml.priority 0.1
  end
  xml.url do
    xml.loc privacy_policy_url
    xml.lastmod ENV["HEROKU_RELEASE_CREATED_AT"]
    xml.changefreq "monthly"
    xml.priority 0.1
  end
end
