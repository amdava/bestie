# frozen_string_literal: true

class Invitation < ApplicationRecord
  belongs_to :requester, class_name: "User"
  has_one :friendship, dependent: :destroy
  validates :email,
    uniqueness: {scope: :requester_id, message: "was already invited"},
    format: {with: Devise.email_regexp,
             message: "doesn't look like an email address"}
  validates :name, presence: true
  validates :invited_at, presence: true

  before_validation :set_defaults

  def accept!
    update_attribute(:accepted_at, Time.zone.now)
    Friendship.create!(user: requester, friend: User.find_by!(email: email), invitation: self)
  end

  def reject!
    update_attribute(:rejected_at, Time.zone.now)
  end

  def email_hash
    Digest::MD5.hexdigest(email)
  end

  def self.open
    where(rejected_at: nil, accepted_at: nil)
  end

  def to_s
    super("#{requester.email}:#{email}")
  end

  def rails_admin_label
    "from #{requester.name} to #{email}"
  end

  rails_admin do
    object_label_method { :rails_admin_label }
    list do
      field :requester
      field :name
      field :email
      field :invited_at
      field :email_sent_at
    end
  end

  private

  def set_defaults
    self.invited_at = Time.zone.now if invited_at.blank?
  end
end

# == Schema Information
#
# Table name: invitations
#
#  id            :uuid             not null, primary key
#  accepted_at   :datetime
#  email         :string           not null
#  email_sent_at :datetime
#  invited_at    :datetime         not null
#  name          :string           not null
#  rejected_at   :datetime
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  requester_id  :uuid             not null
#
# Indexes
#
#  index_invitations_on_requester_id            (requester_id)
#  index_invitations_on_requester_id_and_email  (requester_id,email) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (requester_id => users.id)
#
