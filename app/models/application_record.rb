# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def to_s(extra = nil)
    if extra
      extra = ":#{extra}"
    end
    "#<#{self.class.name}:#{id}#{extra}>"
  end
end
