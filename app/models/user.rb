# frozen_string_literal: true

class User < ApplicationRecord
  TOKEN_LENGTH = 64
  PROMPT_WINDOW = 6.days # How long to way between prompts.
  OK_WAIT_WINDOW = 24.hours # How long to wait from someone to be marked as not ok.

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable, :confirmable, :lockable, :timeoutable, :trackable, :async # :omniauthable

  validates :name, presence: true
  validates :nickname, presence: true
  before_create :set_defaults
  before_validation :populate_names
  has_many :friendships, dependent: :destroy
  has_many :friends, through: :friendships
  has_many :invitations, foreign_key: :requester_id, inverse_of: :requester, dependent: :destroy
  has_one_attached :avatar

  def ok?
    # If there are no oldest unanswered prompts, or the oldest one is withing the ok-wait window, then the user is ok.
    oldest_unanswered_prompt_at.blank? || oldest_unanswered_prompt_at > OK_WAIT_WINDOW.ago
  end

  def ok_but_recently_prompted?
    ok? &&
      latest_prompt_at.present? &&
      latest_prompt_at > OK_WAIT_WINDOW.ago &&
      (latest_status_at.blank? || latest_status_at < latest_prompt_at)
  end

  def needs_prompt?
    latest_interaction_at = [latest_prompt_at, latest_status_at].filter(&:present?).max
    latest_interaction_at.nil? || latest_interaction_at < PROMPT_WINDOW.ago
  end

  def needs_alerting_about?
    !ok? && latest_not_ok_alert_at.blank?
  end

  def pending_invitations
    Invitation.where(email: email).open
  end

  def email_hash
    Digest::MD5.hexdigest(email)
  end

  # https://github.com/heartcombo/devise/issues/1513
  def remember_me
    super.nil? ? "1" : super
  end

  # TODO: this method needs a better name.
  def generate_prompt_data!
    self.latest_prompt_at = Time.zone.now
    if oldest_unanswered_prompt_at.nil?
      self.oldest_unanswered_prompt_at = latest_prompt_at
    end
    generate_log_in_token!(save: false)
    save! validate: false
  end

  def generate_log_in_token!(save: true)
    if log_in_token.blank?
      loop do
        self.log_in_token = SecureRandom.urlsafe_base64(TOKEN_LENGTH)
        if !self.class.where(log_in_token: log_in_token).exists?
          break
        end
      end
      save! validate: false if save
    end
  end

  def generate_not_ok_alert_data!
    self.latest_not_ok_alert_at = Time.zone.now
    save! validate: false
  end

  def update_status!
    self.latest_status_at = Time.zone.now
    self.oldest_unanswered_prompt_at = nil
    self.latest_not_ok_alert_at = nil
    save! validate: false
  end

  def to_s
    super(email)
  end

  rails_admin do
    object_label_method { :email }
    list do
      field :name
      field :email
      field :last_sign_in_at
      field :created_at
      field :locked_at
      field :sign_in_count
    end
  end

  private

  def set_defaults
    self.invites_left = 2
    self.latest_status_at ||= Time.zone.now
  end

  def populate_names
    if name.blank? && email.present?
      self.name = email.split("@").first
    end
    if nickname.blank? && name.present?
      self.nickname = name.split(" ").first
    end
  end
end

# == Schema Information
#
# Table name: users
#
#  id                          :uuid             not null, primary key
#  confirmation_sent_at        :datetime
#  confirmation_token          :string
#  confirmed_at                :datetime
#  current_sign_in_at          :datetime
#  current_sign_in_ip          :string
#  email                       :string           default(""), not null
#  encrypted_password          :string           default(""), not null
#  failed_attempts             :integer          default(0), not null
#  invites_left                :integer          default(0), not null
#  last_sign_in_at             :datetime
#  last_sign_in_ip             :string
#  latest_not_ok_alert_at      :datetime
#  latest_prompt_at            :datetime
#  latest_status_at            :datetime
#  locked_at                   :datetime
#  log_in_token                :string
#  name                        :string           not null
#  nickname                    :string           not null
#  oldest_unanswered_prompt_at :datetime
#  remember_created_at         :datetime
#  reset_password_sent_at      :datetime
#  reset_password_token        :string
#  sign_in_count               :integer          default(0), not null
#  unconfirmed_email           :string
#  unlock_token                :string
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
# Indexes
#
#  index_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_log_in_token          (log_in_token) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_unlock_token          (unlock_token) UNIQUE
#
