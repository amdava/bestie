# frozen_string_literal: true

class Friendship < ApplicationRecord
  belongs_to :user
  belongs_to :friend, class_name: "User"
  belongs_to :invitation
  validate :integrity
  after_create :create_inverse
  after_destroy :delete_inverse

  def to_s
    super("#{user.email}:#{friend.email}")
  end

  def rails_admin_label
    "from #{user.name} to #{friend.name}"
  end

  rails_admin do
    object_label_method { :rails_admin_label }
    list do
      field :user
      field :friend
      field :invitation do
        pretty_value { bindings[:view].link_to(value.id, bindings[:view].show_path(model_name: value.class.name, id: value.id)) }
      end
      field :created_at
    end
  end

  private

  def integrity
    if user == friend
      errors.add(:friend, "cannot be equal to user")
    end
  end

  def create_inverse
    Friendship.find_or_create_by!(user: friend, friend: user) do |inverse|
      inverse.invitation_id = invitation_id
    end
  end

  def delete_inverse
    Friendship.where(user: friend, friend: user).delete_all
  end
end

# == Schema Information
#
# Table name: friendships
#
#  id            :uuid             not null, primary key
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  friend_id     :uuid             not null
#  invitation_id :uuid             not null
#  user_id       :uuid             not null
#
# Indexes
#
#  index_friendships_on_friend_id      (friend_id)
#  index_friendships_on_invitation_id  (invitation_id)
#  index_friendships_on_user_id        (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (friend_id => users.id)
#  fk_rails_...  (invitation_id => invitations.id)
#  fk_rails_...  (user_id => users.id)
#
