# frozen_string_literal: true

class Event < ApplicationRecord
  belongs_to :from, class_name: "User", required: false
  belongs_to :to, class_name: "User", required: false
  belongs_to :record, polymorphic: true, required: false

  validates :name, presence: true

  def rails_admin_label
    "#{name} at #{created_at}"
  end

  rails_admin do
    object_label_method { :rails_admin_label }
    list do
      field :name
      field :from
      field :to
      field :created_at
    end
  end
end

# == Schema Information
#
# Table name: events
#
#  id          :uuid             not null, primary key
#  data        :json
#  name        :string           not null
#  record_type :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  from_id     :uuid
#  record_id   :uuid
#  to_id       :uuid
#
# Indexes
#
#  index_events_on_from_id  (from_id)
#  index_events_on_record   (record_type,record_id)
#  index_events_on_to_id    (to_id)
#
# Foreign Keys
#
#  fk_rails_...  (from_id => users.id)
#  fk_rails_...  (to_id => users.id)
#
