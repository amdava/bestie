class PendingInvitationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_pending_invitation, only: %i[update destroy]

  def index
    @pending_invitations = current_user.pending_invitations.includes(requester: [:avatar_blob, :avatar_attachment])
  end

  # Updating a pending invitation is an implicit accept.
  def update
    @pending_invitation.accept!
    # TODO: send an email about the accepted invitation.
    redirect_back fallback_location: pending_invitations_url, notice: "You are now friends with #{@pending_invitation.requester.name}."
  end

  # Destroying a pending invitation is an implicit reject
  def destroy
    @pending_invitation.reject!
    redirect_back fallback_location: pending_invitations_url, notice: "The invitation from #{@pending_invitation.requester.name} was deleted."
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_pending_invitation
    @pending_invitation = current_user.pending_invitations.find(params[:id])
  end
end
