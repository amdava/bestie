# frozen_string_literal: true

class InvitationsController < ApplicationController
  before_action :authenticate_user!

  def index
    @invitations = current_user.invitations.open
  end

  def create
    @invitation = Invitation.new(invitation_params)
    @invitation.requester = current_user

    if current_user.invites_left < 1
      flash.now[:alert] = "We are sorry, but you don't have any invites left. Send an email to info@bestie.care to ask for more."
      render :new, status: :unprocessable_entity
    elsif @invitation.save
      UserMailer.with(invitation: @invitation).invite.deliver_later
      current_user.update_attribute(:invites_left, current_user.invites_left - 1)
      redirect_to invitations_url, notice: "#{@invitation.name} was invited."
    else
      render :new, status: :unprocessable_entity
    end
  end

  def destroy
    @invitation = current_user.invitations.find(params[:id])
    name = @invitation.name
    @invitation.destroy
    redirect_to invitations_url, notice: "Invitation to #{name} was deleted."
  end

  private

  def invitation_params
    params.require(:invitation).permit(:email, :name)
  end
end
