# frozen_string_literal: true

class StatusController < ApplicationController
  def create
    source = :web
    if params.key?(:token)
      user = User.find_by(log_in_token: params[:token])
      source = :email
      if user
        sign_in user
      end
    end

    if user_signed_in?
      current_user.update_status!
      Event.create! name: :update_status, from: current_user, data: {source: source}
      redirect_back fallback_location: root_url, allow_other_host: false, notice: "We are glad you are OK."
    else
      flash[:alert] = "Something went wrong registering your OK, but you can log in and press the button."
      redirect_to new_user_session_path # not using authenticate_user! to be able to set an alert.
    end
  end
end
