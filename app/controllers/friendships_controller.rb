# frozen_string_literal: true

class FriendshipsController < ApplicationController
  before_action :authenticate_user!

  def index
    @friendships = current_user.friendships.includes(friend: [:avatar_blob, :avatar_attachment])
    @pending_invitations = current_user.pending_invitations.includes(requester: [:avatar_blob, :avatar_attachment])
  end

  def destroy
    @friendship = current_user.friendships.find(params[:id])
    nickname = @friendship.friend.nickname
    @friendship.destroy
    redirect_to friendships_url, notice: "You are no longer friends with #{nickname}"
  end
end
