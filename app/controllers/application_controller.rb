# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :track
  if Rails.env.production? && ENV["PUBLIC"] != "true"
    http_basic_authenticate_with name: "bestie", password: "staging"
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :nickname, :avatar])
  end

  def track
    @tracker = Tracker.new
    data = {
      "$current_url" => request.original_url,
      "$browser" => browser.name,
      "$browser_version" => browser.version,
      "$device" => browser.device.name,
      "$device_id" => browser.device.id,
      "$os" => browser.platform.name
    }
    %w[utm_source utm_medium utm_campaign utm_term utm_content].each do |name|
      data[name] = params[name] if params[name].present?
    end
    session[:c] = true # Force the creation of the session, so there's a session.id
    @tracker.track(current_user&.id || session.id.to_s, "Visit", data)
  end
end
