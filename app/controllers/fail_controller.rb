# frozen_string_literal: true

class FailController < ApplicationController
  def fail
    raise "Bogus exception to test failure"
  end
end
