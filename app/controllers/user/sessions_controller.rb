# frozen_string_literal: true

class User::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  def create
    super
    @tracker.alias(session.id.to_s, current_user.id)
    @tracker.alias(current_user.email, current_user.id)
    data = {
      "$email" => current_user.email,
      "$ip" => request.remote_ip,
      "$name" => current_user.name,
      "$created" => current_user.created_at,
      "$browser" => browser.name,
      "$browser_version" => browser.version,
      "$os" => browser.platform.name
    }
    data["$avatar"] = current_user.avatar if current_user.avatar.present?
    @tracker.set_user_data(current_user.id, data)
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
