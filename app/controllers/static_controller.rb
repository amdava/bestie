# frozen_string_literal: true

class StaticController < ApplicationController
  def home
    if user_signed_in?
      redirect_to friendships_url
    end
  end
end
