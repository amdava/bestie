# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: "Bestie <info@bestie.care>"
  layout "mailer"
  helper :application
end
