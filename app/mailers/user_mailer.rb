# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def invite
    @invitation = params[:invitation]
    @invitee = User.find_by(email: @invitation.email)
    @invitee&.generate_log_in_token!
    @invitation.update_attribute(:email_sent_at, Time.zone.now)
    Event.create! name: :invite, from: @invitation.requester, to: @invitee, record: @invitation, data: {to_name: @invitation.name, to_email: @invitation.name}
    Tracker.track(@invitation.requester.id, "Sent Invitation", {id: @invitation.id, to_name: @invitation.name, to_email: @invitation.name, to_user_id: @invitee&.id})
    Tracker.track(@invitee&.id || @invitation.email, "Received Invitation", {id: @invitation.id, from_name: @invitation.requester.name, from_email: @invitation.requester.email, from_user_id: @invitation.requester.id})
    mail to: %("#{@invitation.name}" <#{@invitation.email}>),
         subject: "#{@invitation.requester.name} added you on Bestie"
  end

  def prompt
    @user = params[:user]
    @user.generate_prompt_data!
    Tracker.track(@user.id, "Sent Prompt")
    Event.create! name: :prompt, to: @user
    mail to: %("#{@user.name}" <#{@user.email}>),
         subject: "Hey #{@user.nickname}, are you OK?"
  end

  def not_ok_alert
    @user = params[:user]
    @not_ok_user = params[:not_ok_user]
    @not_ok_user.generate_not_ok_alert_data!
    Tracker.track(@user.id, "Sent Not OK Alert", {about_user_id: @not_ok_user.id, about_user_name: @not_ok_user.name, about_user_email: @not_ok_user.email})
    Event.create! name: :not_ok_alert, from: @user, to: @not_ok_user
    mail to: %("#{@user.name}" <#{@user.email}>),
         subject: "#{@not_ok_user.nickname} might need your help"
  end
end
