module FriendshipsHelper
  def friendship_color(user)
    if user.ok?
      if user.ok_but_recently_prompted?
        "bg-yellow-300"
      else
        "bg-green-400"
      end
    else
      "bg-red-500"
    end
  end

  def friendship_message(user)
    style = ""
    message = nil
    if user.ok?
      if user.ok_but_recently_prompted?
        message = if user.latest_status_at
          "#{user.nickname} last said they were ok #{time_ago_in_words(user.latest_status_at)} ago, we sent them another prompt recently, #{time_ago_in_words(user.latest_prompt_at)} ago."
        else
          "We last reached out to #{user.nickname} #{time_ago_in_words(user.latest_prompt_at)} ago we are still waiting."
        end
      elsif user.latest_status_at
        message = "#{user.nickname} said they were ok #{time_ago_in_words(user.latest_status_at)} ago."
      end
    else
      style = "p-1.5
               bg-red-100 border-l-4 border-red-700 text-red-500
               hover:bg-red-200
               focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-red-100 focus:ring-red-600"
      message = "#{user.nickname} may not be ok, you should reach out. We ask them #{time_ago_in_words(user.oldest_unanswered_prompt_at)} ago and we are still waiting."
    end

    if message
      content_tag(:div, class: style) { message }
    end
  end
end
