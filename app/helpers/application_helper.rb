# frozen_string_literal: true

module ApplicationHelper
  def gravatar_url(id, size: 80)
    "https://www.gravatar.com/avatar/#{id}?s=#{size}&d=wavatar"
  end

  def fa(*args)
    classes = ["fa"]
    args.each do |arg|
      classes << if arg.is_a? Symbol
        "fa-#{arg.to_s.tr("_", "-")}"
      else
        arg
      end
    end
    content_tag(:i, class: classes.join(" "), aria: {hidden: true}) {}
  end

  def fab(*args)
    classes = ["fab"]
    args.each do |arg|
      classes << if arg.is_a? Symbol
        "fa-#{arg.to_s.tr("_", "-")}"
      else
        arg
      end
    end
    content_tag(:i, class: classes.join(" "), aria: {hidden: true}) {}
  end

  def drop_down_menu_item_class
    "group inline w-full flex px-4 py-2
     items-center
     text-sm leading-5 text-gray-700
     hover:bg-gray-300 hover:text-gray-900
     focus:outline-none focus:bg-gray-300 focus:text-gray-900"
  end

  def link_class
    "font-medium text-green-600 hover:text-green-500"
  end

  def button_class
    "flex-grow inline-flex py-2 px-4
     items-center justify-center
     bg-green-600 border border-transparent rounded-md
     text-sm leading-5 font-medium text-white
     transition duration-150 ease-in-out
     hover:bg-green-500 active:bg-green-700
     focus:border-green-700 focus:outline-none focus:shadow-outline-green"
  end

  def utm(url, source: "bestie", medium: "web", campaign: nil, term: nil, content: nil)
    url = URI.parse(url)
    new_query = URI.decode_www_form(url.query.to_s)
    new_query << ["utm_source", source] if source.present?
    new_query << ["utm_medium", medium] if medium.present?
    new_query << ["utm_campaign", campaign] if campaign.present?
    new_query << ["utm_term", term] if term.present?
    new_query << ["utm_content", content] if content.present?
    url.query = URI.encode_www_form(new_query)
    url.to_s
  end
end
