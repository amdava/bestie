# frozen_string_literal: true

module FormHelper
  def form_label_class(error = false)
    classes = "block text-sm font-medium "
    classes += if error
      "text-red-600"
    else
      "text-gray-700"
    end
    classes
  end

  def form_input_class(error = false)
    classes = "appearance-none block w-full px-3 py-2 border rounded-md shadow-sm focus:outline-none sm:text-sm "
    classes += if error
      "text-red-900 border-red-300 placeholder-red-300 focus:ring-red-500 focus:border-red-500"
    else
      "border-gray-300 placeholder-gray-400 focus:ring-green-500 focus:border-green-500"
    end
    classes
  end

  def form_submit_class
    "w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 cursor-pointer"
  end
end
