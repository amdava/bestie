# frozen_string_literal: true

module FooterHelper
  def footer_link_class
    "text-base text-gray-500 hover:text-gray-900"
  end

  def footer_link(*args, &block)
    args[args.count - 1] = utm(args.last, content: "footer")
    link_to(*args, class: footer_link_class, &block)
  end
end
