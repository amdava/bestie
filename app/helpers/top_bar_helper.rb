# frozen_string_literal: true

module TopBarHelper
  def top_bar_desktop_link(*args, &block)
    args[args.count - 1] = utm(args.last, content: "top_bar_desktop")
    active_link_to(*args,
      class: "px-3 py-2 rounded-md text-sm font-medium",
      class_inactive: "text-green-300 hover:bg-green-700 hover:text-white",
      class_active: "bg-green-900 text-white",
      role: "menuitem",
      &block)
  end

  def top_bar_mobile_link_class
    "block px-3 py-2 rounded-md text-base font-medium"
  end

  def top_bar_mobile_link_inactive_class
    "text-gray-300 hover:bg-green-700 hover:text-white"
  end

  def top_bar_mobile_button_class
    "w-full bg-transparent text-left cursor-pointer #{top_bar_mobile_link_class} #{top_bar_mobile_link_inactive_class}"
  end

  def top_bar_mobile_link(*args, &block)
    args[args.count - 1] = utm(args.last, content: "top_bar_mobile")
    active_link_to(*args,
      class: top_bar_mobile_link_class,
      class_inactive: top_bar_mobile_link_inactive_class,
      class_active: "bg-green-900 text-white",
      role: "menuitem",
      &block)
  end
end
