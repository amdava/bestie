# frozen_string_literal: true

require "rails_helper"

RSpec.describe UserMailer, type: :mailer do
  describe "invite" do
    it "invites a new user" do
      invitation = create(:invitation)
      mail = described_class.with(invitation: invitation).invite
      expect(mail.subject).to eq("#{invitation.requester.name} added you on Bestie")
      expect(mail.to).to eq([invitation.email])
      expect(mail.from).to eq(["info@bestie.care"])
      expect(mail.body).to match("Hello #{ERB::Util.html_escape invitation.name}")
    end

    it "invites an existing user" do
      invitation = create(:invitation)
      create(:user, email: invitation.email, name: invitation.name)
      mail = described_class.with(invitation: invitation).invite
      expect(mail.subject).to eq("#{invitation.requester.name} added you on Bestie")
      expect(mail.to).to eq([invitation.email])
      expect(mail.from).to eq(["info@bestie.care"])
      expect(mail.body).to match("Hello #{ERB::Util.html_escape invitation.name}")
    end
  end
end
