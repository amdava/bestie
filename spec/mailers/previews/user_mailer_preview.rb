# frozen_string_literal: true

# Preview all emails at http://localhost:3001/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  include FactoryBot::Syntax::Methods

  def invite_to_existing_user
    invitation = create(:invitation)
    create(:user, name: invitation.name, email: invitation.email)
    UserMailer.with(invitation: invitation).invite
  end

  def invite_to_new_user
    invitation = create(:invitation)
    UserMailer.with(invitation: invitation).invite
  end

  def prompt_new_user_with_no_friends
    user = create(:user)
    UserMailer.with(user: user).prompt
  end

  def prompt_new_user_with_one_friends
    user = create(:user)
    create(:friendship, user: user)
    UserMailer.with(user: user).prompt
  end

  def prompt_new_user_with_two_friends
    user = create(:user)
    2.times { create(:friendship, user: user) }
    UserMailer.with(user: user).prompt
  end

  def prompt_new_user_with_five_friends
    user = create(:user)
    5.times { create(:friendship, user: user) }
    UserMailer.with(user: user).prompt
  end

  def not_ok_alert
    not_ok_user = create(:user)
    Timecop.freeze(10.days.ago) { not_ok_user.generate_prompt_data! }
    friend = create(:friendship, user: not_ok_user).friend
    UserMailer.with(not_ok_user: not_ok_user, user: friend).not_ok_alert
  end
end
