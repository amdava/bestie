# frozen_string_literal: true

require "rails_helper"

RSpec.describe "/users", type: :request do
  it "works" do
    get "/users/sign_up"
    expect(response).to have_http_status(:success)
    email = Faker::Internet.email
    post "/users", params: {
      user: {
        name: Faker::Name.name,
        email: email,
        password: "password1000",
        password_confirmation: "password1000"
      }
    }
    expect(response).to redirect_to(root_url)
  end
end
