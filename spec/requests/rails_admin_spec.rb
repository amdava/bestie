# frozen_string_literal: true

require "rails_helper"

RSpec.describe "/admin", type: :request do
  it "works" do
    create(:admin, email: "admin@example.com", password: "password")

    # Log in
    get "/admin"
    expect(response).to redirect_to(new_admin_session_path)
    post "/admins/sign_in", params: {admin: {email: "admin@example.com", password: "password"}}
    expect(response).to redirect_to("/admin/")
    get "/admin"
    expect(response).to have_http_status(:success)
    expect(response.body).to include("Bestie").and include("Admin")

    # Just walk through all models.
    get "/admin/admin"
    expect(response).to have_http_status(:success)
    get "/admin/event"
    expect(response).to have_http_status(:success)
    get "/admin/friendship"
    expect(response).to have_http_status(:success)
    get "/admin/invitation"
    expect(response).to have_http_status(:success)
    get "/admin/user"
    expect(response).to have_http_status(:success)
    get "/admin/active_storage~attachment"
    expect(response).to have_http_status(:success)
    get "/admin/active_storage~blob"
    expect(response).to have_http_status(:success)
    get "/admin/active_storage~variant_record"
    expect(response).to have_http_status(:success)
    get "/admin/delayed~backend~active_record~job"
    expect(response).to have_http_status(:success)
    get "/admin/active_record~session_store~session"
    expect(response).to have_http_status(:success)
  end
end
