# frozen_string_literal: true

require "rails_helper"

RSpec.describe "/status", type: :request do
  it "doesn't create a status when not logged in" do
    get "/status"

    expect(response).to redirect_to(new_user_session_path)
  end

  it "doesn't create a status when the token is wrong" do
    get "/status", params: {token: "giberish"}

    expect(response).to redirect_to(new_user_session_path)
  end

  context "with a user" do
    before { @user = create(:user) }

    it "creates a status from a prompt" do
      @user.generate_prompt_data!

      get "/status", params: {token: @user.log_in_token}

      @user.reload
      expect(@user.latest_status_at).to be_within(15.minutes).of(Time.zone.now)
      expect(response).to redirect_to(root_url)
    end

    it "creates a status while signed in" do
      sign_in @user

      get "/status"

      @user.reload
      expect(@user.latest_status_at).to be_within(15.minutes).of(Time.zone.now)
      expect(response).to redirect_to(root_url)
    end
  end
end
