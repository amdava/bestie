# frozen_string_literal: true

require "rails_helper"

RSpec.describe "/pending_invitations", type: :request do
  context "when not logged in" do
    it "doesn't list" do
      get "/pending_invitations"

      expect(response).to redirect_to(new_user_session_path)
    end

    context "with an invitation" do
      before do
        @invitation = create(:invitation)
      end

      it "doesn't accept invite" do
        expect do
          patch "/pending_invitations/#{@invitation.id}"
        end.to change(Friendship, :count).by(0)
          .and change(Invitation, :count).by(0)

        expect(response).to redirect_to(new_user_session_path)
      end

      it "doesn't reject invite" do
        expect do
          delete "/pending_invitations/#{@invitation.id}"
        end.to change(Friendship, :count).by(0)
          .and change(Invitation, :count).by(0)

        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  context "when logged in" do
    before do
      @user = create(:user)
      sign_in @user
    end

    it "gets a list of invitations" do
      invitations = create_list(:invitation, 10, email: @user.email) # Invitations to me, the ones that should be seen.
      create_list(:invitation, 10) # Random invitations, not to be seen.
      create_list(:invitation, 10, requester: @user) # Invitations from me, not to be seen here.

      get "/pending_invitations"

      expect(response).to have_http_status(:ok)
      invitations.each do |invitation|
        expect(response.body).to include(invitation.id)
      end
    end

    context "with an invitation" do
      before do
        @invitation = create(:invitation, email: @user.email, name: @user.name)
      end

      it "accepts invite" do
        expect do
          patch "/pending_invitations/#{@invitation.id}"
        end.to change { @user.friendships.count }.by(1)
          .and change(Friendship, :count).by(+2)
          .and change { @user.invitations.count }.by(0)
          .and change { @user.pending_invitations.count }.by(-1)
          .and change(Invitation, :count).by(0)

        expect(response).to redirect_to(pending_invitations_url)
        @invitation.reload
        expect(@invitation.accepted_at).not_to be_blank
        expect(@invitation.rejected_at).to be_blank
        expect(@invitation.friendship).not_to be_blank
      end

      it "rejects invite" do
        expect do
          delete "/pending_invitations/#{@invitation.id}"
        end.to change { @user.friendships.count }.by(0)
          .and change(Friendship, :count).by(0)
          .and change { @user.invitations.count }.by(0)
          .and change { @user.pending_invitations.count }.by(-1)
          .and change(Invitation, :count).by(0)

        expect(@response).to redirect_to(pending_invitations_url)
        @invitation.reload
        expect(@invitation.accepted_at).to be_blank
        expect(@invitation.rejected_at).not_to be_blank
        expect(@invitation.friendship).to be_blank
      end
    end

    context "with someone else's invitation" do
      before do
        @other_invitation = create(:invitation)
      end

      it "fails to accept someone else's invitation" do
        expect do
          patch "/pending_invitations/#{@other_invitation.id}"
        end.to change { @user.friendships.count }.by(0)
          .and change(Friendship, :count).by(0)
          .and change { @user.invitations.count }.by(0)
          .and change(Invitation, :count).by(0)
          .and raise_exception(ActiveRecord::RecordNotFound)

        @other_invitation.reload
        expect(@other_invitation.accepted_at).to be_blank
        expect(@other_invitation.rejected_at).to be_blank
        expect(@other_invitation.friendship).to be_blank
      end

      it "fails to reject someone else's invitation" do
        expect do
          delete "/pending_invitations/#{@other_invitation.id}"
        end.to change { @user.friendships.count }.by(0)
          .and change(Friendship, :count).by(0)
          .and change { @user.invitations.count }.by(0)
          .and change(Invitation, :count).by(0)
          .and raise_exception(ActiveRecord::RecordNotFound)

        @other_invitation.reload
        expect(@other_invitation.accepted_at).to be_blank
        expect(@other_invitation.rejected_at).to be_blank
        expect(@other_invitation.friendship).to be_blank
      end
    end
  end
end
