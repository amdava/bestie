# frozen_string_literal: true

require "rails_helper"

RSpec.describe "/friendships", type: :request do
  before do
    @user = create(:user, :with_all_friendships)
    @friendship = create(:friendship, user: @user)
  end

  it "is readable" do
    expect(@friendship.to_s).to include(@friendship.id)
    expect(@friendship.rails_admin_label).to include(@friendship.user.name)
    expect(@friendship.rails_admin_label).to include(@friendship.friend.name)
  end

  context "when not logged in" do
    it "doesn't list" do
      get "/friendships"
      expect(response).to redirect_to(new_user_session_path)
    end

    it "doesn't delete" do
      delete "/friendships/#{@friendship.id}"
      expect(response).to redirect_to(new_user_session_path)
    end
  end

  context "when logged in" do
    before { sign_in @user }

    it "gets a list of friends" do
      friendships_a = create_list(:friendship, 5, user: @user) # The friendships that should be seen.
      friendships_b = create_list(:friendship, 5, friend: @user) # The friendships that should be seen.
      create_list(:friendship, 10) # Random other friendships
      friendships_a.take(4).each do |friendship|
        Timecop.freeze(3.days.ago) { friendship.friend.update_status! }
      end
      friendships_b.take(4).each do |friendship|
        Timecop.freeze(3.days.ago) { friendship.user.update_status! }
      end

      get "/friendships"

      expect(response).to have_http_status(:ok)
      # Make sure the other users are listed as a friendship, whether they are user or friend
      friendships_a.each do |friendship|
        expect(response.body).to include(CGI.escapeHTML(friendship.friend.name))
      end
      friendships_b.each do |friendship|
        expect(response.body).to include(CGI.escapeHTML(friendship.user.name))
      end
    end

    it "deletes a friendship" do
      expect do
        delete "/friendships/#{@friendship.id}"
      end.to change { @user.friendships.count }.by(-1)
        .and change(Friendship, :count).by(-2)
      expect(response).to have_http_status(:found)
      expect(Friendship.exists?(id: @friendship.id)).to be(false)
    end

    it "doesn't delete a friendship in the opposite direction" do
      opposite_friendship = Friendship.where(user: @friendship.friend, friend: @user).first!
      expect do
        delete "/friendships/#{opposite_friendship.id}"
      end.to change { @user.friendships.count }.by(0)
        .and change(Friendship, :count).by(0)
        .and raise_exception(ActiveRecord::RecordNotFound)
      expect(Friendship.exists?(id: @friendship.id)).to be(true)
      expect(Friendship.exists?(id: opposite_friendship.id)).to be(true)
    end

    it "doesn't delete someone else's friendship" do
      other_friendship = create(:friendship) # Not requested by user.
      expect do
        delete "/friendships/#{other_friendship.id}"
      end.to change { @user.friendships.count }.by(0)
        .and change(Friendship, :count).by(0)
        .and raise_exception(ActiveRecord::RecordNotFound)
      expect(Friendship.exists?(id: other_friendship.id)).to be(true)
    end
  end
end
