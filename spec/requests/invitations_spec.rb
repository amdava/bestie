# frozen_string_literal: true

require "rails_helper"

RSpec.describe "/invitations", type: :request do
  context "when not logged in" do
    it "doesn't list invitations" do
      get "/invitations"
      expect(response).to redirect_to(new_user_session_path)
    end

    it "doesn't create an invitation" do
      post "/invitations", params: {invitation: {email: "julia.roberts@example.com", name: "Julia"}}
      expect(response).to redirect_to(new_user_session_path)
    end

    it "doesn't delete an invitation" do
      invitation = create(:invitation)
      delete "/invitations/#{invitation.id}"
      expect(response).to redirect_to(new_user_session_path)
    end
  end

  context "when logged in" do
    before do
      @user = create(:user)
      sign_in @user
    end

    it "gets a list of invitations" do
      invitations = create_list(:invitation, 10, requester: @user) # The invitations that should be seen.
      create_list(:invitation, 10) # Random other invitations
      create_list(:invitation, 10, email: @user.email) # Invitations to me, not from me.

      get "/invitations"

      expect(response).to have_http_status(:ok)
      invitations.each do |invitation|
        expect(response.body).to include(invitation.id)
      end
    end

    it "creates an invitation" do
      name = Faker::Name.unique.name
      email = "#{name.downcase.gsub(/[\s.]+/, ".")}@#{Faker::Internet.unique.domain_name}"

      create(:invitation, email: email, name: name) # Someone else's invitation to the same person, to test they won't collide.

      expect do
        post "/invitations", params: {invitation: {email: email, name: name}}
        @user.reload
      end.to change(Invitation, :count).by(1)
        .and change { @user.invites_left }.by(-1)

      expect(response).to redirect_to(invitations_path)
      follow_redirect!
      expect(response.body)
        .to include(email)
        .and include(name)
    end

    it "fails to create invitation due to validations" do
      expect do
        post "/invitations", params: {invitation: {email: "", name: ""}}
      end.to change(Invitation, :count).by(0)

      expect(response).to have_http_status(:unprocessable_entity)
      expect(response.body).to include("Email doesn&#39;t look like an email address")
        .and include("Name can&#39;t be blank")
    end

    it "fails to create invitation due to lack of invites left" do
      @user.invites_left = 0
      @user.save!
      name = Faker::Name.unique.name
      email = "#{name.downcase.gsub(/[\s.]+/, ".")}@#{Faker::Internet.unique.domain_name}"

      expect do
        post "/invitations", params: {invitation: {email: email, name: name}}
      end.to change(Invitation, :count).by(0)

      expect(response).to have_http_status(:unprocessable_entity)
      expect(response.body).to include("We are sorry, but you don&#39;t have any invites left. Send an email to info@bestie.care to ask for more.")
    end

    it "doesn't delete someone else's invitation" do
      other_invitation = create(:invitation) # Not requested by @user.

      expect do
        delete "/invitations/#{other_invitation.id}"
      end.to change(Invitation, :count).by(0)
        .and raise_exception(ActiveRecord::RecordNotFound)

      expect(Invitation.exists?(id: other_invitation.id)).to be(true)
    end

    context "with an invitation" do
      before do
        @invitation = create(:invitation, requester: @user)
      end

      it "fails to create invitation due to clashing email" do
        expect do
          post "/invitations", params: {invitation: {email: @invitation.email, name: @invitation.name}}
        end.to change(Invitation, :count).by(0)

        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.body).to include("was already invited")
      end

      it "deletes it" do
        expect do
          delete "/invitations/#{@invitation.id}"
        end.to change(Invitation, :count).by(-1)

        expect(response).to have_http_status(:found)
        expect(Invitation.exists?(id: @invitation.id)).to be(false)
      end
    end
  end
end
