# frozen_string_literal: true

require "rails_helper"

RSpec.describe "/", type: :request do
  it "shows homepage" do
    get "/"
    expect(response).to have_http_status(:success)
    expect(response.body)
      .to include("Register")
      .and include("get help")
  end

  it "redirects from homepage when logged in" do
    @user = create(:user)
    sign_in @user

    get "/"

    expect(response).to redirect_to(friendships_url)
  end

  it "shows faq" do
    get "/faq"
    expect(response).to have_http_status(:success)
    expect(response.body)
      .to include("Frequently Asked Questions")
      .and include("How does this work?")
  end

  it "shows pricing" do
    get "/pricing"
    expect(response).to have_http_status(:success)
    expect(response.body)
      .to include("FREE")
      .and include("Contact Us")
  end

  it "shows privacy policy" do
    get "/privacy-policy"
    expect(response).to have_http_status(:success)
    expect(response.body)
      .to include("Privacy Policy")
      .and include("We use the information we collect in various ways")
  end

  it "shows cookie policy" do
    get "/cookie-policy"
    expect(response).to have_http_status(:success)
    expect(response.body)
      .to include("Cookie Policy")
      .and include("If you create an account")
  end
end
