# frozen_string_literal: true

require "rails_helper"

RSpec.describe "/fail", type: :request do
  it "fails" do
    expect do
      get "/fail"
    end.to raise_error(RuntimeError, "Bogus exception to test failure")
  end
end
