# frozen_string_literal: true

FactoryBot.define do
  factory :friendship do
    association(:user, factory: :user)
    association(:friend, factory: :user)
    invitation do
      build(:invitation,
        requester: user,
        name: friend.name,
        email: friend.email,
        email_sent_at: 10.hours.ago,
        accepted_at: Time.zone.now)
    end
  end
end

# == Schema Information
#
# Table name: friendships
#
#  id            :uuid             not null, primary key
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  friend_id     :uuid             not null
#  invitation_id :uuid             not null
#  user_id       :uuid             not null
#
# Indexes
#
#  index_friendships_on_friend_id      (friend_id)
#  index_friendships_on_invitation_id  (invitation_id)
#  index_friendships_on_user_id        (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (friend_id => users.id)
#  fk_rails_...  (invitation_id => invitations.id)
#  fk_rails_...  (user_id => users.id)
#
