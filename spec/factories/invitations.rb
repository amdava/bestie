# frozen_string_literal: true

FactoryBot.define do
  factory :invitation do
    association(:requester, factory: :user)
    name { Faker::Name.unique.name }
    email { "#{name.downcase.gsub(/[\s.]+/, ".")}@#{Faker::Internet.unique.domain_name}" }
  end
end

# == Schema Information
#
# Table name: invitations
#
#  id            :uuid             not null, primary key
#  accepted_at   :datetime
#  email         :string           not null
#  email_sent_at :datetime
#  invited_at    :datetime         not null
#  name          :string           not null
#  rejected_at   :datetime
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  requester_id  :uuid             not null
#
# Indexes
#
#  index_invitations_on_requester_id            (requester_id)
#  index_invitations_on_requester_id_and_email  (requester_id,email) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (requester_id => users.id)
#
