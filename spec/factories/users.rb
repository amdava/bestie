# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    name { Faker::Name.unique.name }
    email { "#{name.downcase.gsub(/[\s.]+/, ".")}@#{Faker::Internet.unique.domain_name}" }
    password { "TeZlk27HIp403YwthqzrRTrs6" }
    confirmed_at { Time.zone.now }
    invites_left { 10 }

    before(:create) do |user, _evaluator|
      user.skip_confirmation_notification!
    end

    trait :with_avatar do
      after(:create) do |user, _evaluator|
        file_name = "#{user.name.downcase.gsub(/[\s.]+/, "_")}.jpg"
        user.avatar.attach(io: File.open("#{Rails.root}/spec/factories/users/avatars/#{file_name}"), filename: file_name)
      end
    end

    trait :with_all_friendships do
      after(:create) do |user, _evaluator|
        print "Creating all friend combinations for #{user.name}..." if $verbose
        create(:friendship, user: create(:user, :brad_pitt, :with_avatar), friend: user)
        print "." if $verbose
        create(:friendship, user: create(:user, :scarlett_johansson, :with_avatar), friend: user)
        print "." if $verbose
        create(:friendship, user: create(:user, :leonardo_dicaprio, :with_avatar), friend: user)
        print "." if $verbose
        create(:friendship, user: create(:user, :cate_blanchett, :with_avatar), friend: user)
        print "." if $verbose
        create(:friendship, user: create(:user, :robert_de_niro, :with_avatar), friend: user)
        print "." if $verbose
        create(:friendship, user: create(:user, :gal_gadot, :with_avatar), friend: user)
        print "." if $verbose
        create(:friendship, user: create(:user, :al_pacino, :with_avatar), friend: user)
        print "." if $verbose
        create(:friendship, user: create(:user, :salma_hayek, :with_avatar), friend: user)
        print "." if $verbose
        create(:friendship, user: create(:user, :harrison_ford, :with_avatar), friend: user)
        print "." if $verbose
        create(:friendship, user: create(:user, :milla_jovovich, :with_avatar), friend: user)
        print "." if $verbose
        puts " done." if $verbose
      end
    end

    trait :brad_pitt do # Brad Pitt has a has first prompt.
      name { "Brad Pitt" }
      latest_status_at { 10.hours.ago }
      after :create do |user, _evaluator|
        Timecop.freeze(2.hours.ago) { user.generate_prompt_data! }
      end
    end

    trait :scarlett_johansson do # Scarlett Johansson has first prompt and is expired
      name { "Scarlett Johansson" }
      latest_status_at { 40.hours.ago }
      after :create do |user, _evaluator|
        Timecop.freeze(30.hours.ago) { user.generate_prompt_data! }
      end
    end

    trait :leonardo_dicaprio do # Leonardo DiCaprio has first prompt, is expired and alerted about
      name { "Leonardo DiCaprio" }
      latest_status_at { 40.hours.ago }
      after :create do |user, _evaluator|
        Timecop.freeze(30.hours.ago) { user.generate_prompt_data! }
        Timecop.freeze(6.hours.ago) { user.generate_not_ok_alert_data! }
      end
    end

    trait :cate_blanchett do # Cate Blanchett has first prompt and first status
      name { "Cate Blanchett" }
      latest_status_at { 40.hours.ago }
      after :create do |user, _evaluator|
        Timecop.freeze(30.hours.ago) { user.generate_prompt_data! }
        Timecop.freeze(1.hour.ago) { user.update_status! }
      end
    end

    trait :robert_de_niro do # Robert De Niro has first prompt, first status, ready for second cycle
      name { "Robert De Niro" }
      latest_status_at { 11.days.ago }
      after :create do |user, _evaluator|
        Timecop.freeze(10.days.ago) { user.generate_prompt_data! }
        Timecop.freeze(9.days.ago) { user.update_status! }
      end
    end

    trait :gal_gadot do # Gal Gadot is on second prompt, but ok
      name { "Gal Gadot" }
      latest_status_at { 11.days.ago }
      after :create do |user, _evaluator|
        Timecop.freeze(10.days.ago) { user.generate_prompt_data! }
        Timecop.freeze(9.days.ago) { user.update_status! }
        Timecop.freeze(2.hours.ago) { user.generate_prompt_data! }
      end
    end

    trait :al_pacino do # Al Pacino is on second prompt, not ok
      name { "Al Pacino" }
      latest_status_at { 11.days.ago }
      after :create do |user, _evaluator|
        Timecop.freeze(10.days.ago) { user.generate_prompt_data! }
        Timecop.freeze(9.days.ago) { user.update_status! }
        Timecop.freeze(30.hours.ago) { user.generate_prompt_data! }
      end
    end

    trait :salma_hayek do # Salma Hayek is on second status, ok
      name { "Salma Hayek" }
      latest_status_at { 11.days.ago }
      after :create do |user, _evaluator|
        Timecop.freeze(10.days.ago) { user.generate_prompt_data! }
        Timecop.freeze(9.days.ago) { user.update_status! }
        Timecop.freeze(10.hours.ago) { user.generate_prompt_data! }
        Timecop.freeze(1.hour.ago) { user.update_status! }
      end
    end

    trait :harrison_ford do # Harrison Ford has two prompts, neither answered
      name { "Harrison Ford" }
      latest_status_at { 11.days.ago }
      after :create do |user, _evaluator|
        Timecop.freeze(10.days.ago) { user.generate_prompt_data! }
        Timecop.freeze(30.hours.ago) { user.generate_prompt_data! }
      end
    end

    trait :milla_jovovich do # Milla Jovovich has two prompts, neither answered, but was ok before
      name { "Milla Jovovich" }
      latest_status_at { 21.days.ago }
      after :create do |user, _evaluator|
        Timecop.freeze(20.days.ago) { user.generate_prompt_data! }
        Timecop.freeze(19.days.ago) { user.update_status! }
        Timecop.freeze(10.days.ago) { user.generate_prompt_data! }
        Timecop.freeze(30.hours.ago) { user.generate_prompt_data! }
      end
    end
  end
end

# == Schema Information
#
# Table name: users
#
#  id                          :uuid             not null, primary key
#  confirmation_sent_at        :datetime
#  confirmation_token          :string
#  confirmed_at                :datetime
#  current_sign_in_at          :datetime
#  current_sign_in_ip          :string
#  email                       :string           default(""), not null
#  encrypted_password          :string           default(""), not null
#  failed_attempts             :integer          default(0), not null
#  invites_left                :integer          default(0), not null
#  last_sign_in_at             :datetime
#  last_sign_in_ip             :string
#  latest_not_ok_alert_at      :datetime
#  latest_prompt_at            :datetime
#  latest_status_at            :datetime
#  locked_at                   :datetime
#  log_in_token                :string
#  name                        :string           not null
#  nickname                    :string           not null
#  oldest_unanswered_prompt_at :datetime
#  remember_created_at         :datetime
#  reset_password_sent_at      :datetime
#  reset_password_token        :string
#  sign_in_count               :integer          default(0), not null
#  unconfirmed_email           :string
#  unlock_token                :string
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
# Indexes
#
#  index_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_log_in_token          (log_in_token) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_unlock_token          (unlock_token) UNIQUE
#
