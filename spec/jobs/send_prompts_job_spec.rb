# frozen_string_literal: true

require "rails_helper"

RSpec.describe SendPromptsJob, type: :job do
  include ActiveJob::TestHelper

  it "sends prompts" do
    user1 = user2 = user3 = user4 = nil
    Timecop.freeze(User::PROMPT_WINDOW.ago - 1.day) do
      user1 = create(:user) # Should get a prompt because they never had any.
      user2 = create(:user) # Should get a prompt because...
      user2.generate_prompt_data! # they haven't had one in a while
    end
    Timecop.freeze(User::PROMPT_WINDOW.ago + 1.day) do
      user3 = create(:user) # Should not get a prompt because ...
      user3.generate_prompt_data! # they got one within the prompt window.
    end
    Timecop.freeze(10.hours.ago) do
      user4 = create(:user) # Should notget a prompt because they are a new user.
    end

    SendPromptsJob.perform_later
    expect { run_all_jobs }.to change(ActionMailer::Base.deliveries, :count).by(2)

    user1.reload
    expect(user1.latest_prompt_at).to be_within(15.minutes).of(Time.zone.now)
    user2.reload
    expect(user2.latest_prompt_at).to be_within(15.minutes).of(Time.zone.now)
    user3.reload
    expect(user3.latest_prompt_at).not_to be_within(15.minutes).of(Time.zone.now)
    user4.reload
    expect(user3.latest_prompt_at).not_to be_within(15.minutes).of(Time.zone.now)
  end
end
