# frozen_string_literal: true

require "rails_helper"

RSpec.describe SendNotOkAlertsJob, type: :job do
  include ActiveJob::TestHelper

  context "with a user not ok" do
    before do
      @not_ok_user = create(:user)
      Timecop.freeze(10.days.ago) { @not_ok_user.generate_prompt_data! }
      @friend = create(:friendship, user: @not_ok_user).friend
    end

    it "sends not-ok alerts" do
      SendNotOkAlertsJob.perform_later
      expect { run_all_jobs }.to change(ActionMailer::Base.deliveries, :count).by(1)
      email = ActionMailer::Base.deliveries.last
      expect(email.to).to include(@friend.email)
      expect(email.body).to include(CGI.escapeHTML(@not_ok_user.name))
      expect(email.body).to include(CGI.escapeHTML(@not_ok_user.email))
      @not_ok_user.reload
      expect(@not_ok_user.latest_not_ok_alert_at).to be_within(15.minutes).of(Time.zone.now)
    end

    it "doesn't send not-ok alert" do
      @not_ok_user.update_status!
      SendNotOkAlertsJob.perform_later
      expect { run_all_jobs }.to change(ActionMailer::Base.deliveries, :count).by(0)
      @not_ok_user.reload
      expect(@not_ok_user.latest_not_ok_alert_at).to be(nil)
    end

    it "doesn't send the same alert twice" do
      SendNotOkAlertsJob.perform_later
      expect { run_all_jobs }.to change(ActionMailer::Base.deliveries, :count).by(1)
      @not_ok_user.reload
      expect(@not_ok_user.latest_not_ok_alert_at).to be_within(15.minutes).of(Time.zone.now)
      SendNotOkAlertsJob.perform_later
      expect { run_all_jobs }.to change(ActionMailer::Base.deliveries, :count).by(0)
    end

    it "doesn't send an alert immediately after a prompt." do
      Timecop.freeze(8.days.ago) do
        SendNotOkAlertsJob.perform_later
        expect { run_all_jobs }.to change(ActionMailer::Base.deliveries, :count).by(1)
        @not_ok_user.reload
        expect(@not_ok_user.latest_not_ok_alert_at).to be_within(15.minutes).of(Time.zone.now)
      end
      @not_ok_user.generate_prompt_data!
      SendNotOkAlertsJob.perform_later
      expect { run_all_jobs }.to change(ActionMailer::Base.deliveries, :count).by(0)
      @not_ok_user.reload
      expect(@not_ok_user.latest_not_ok_alert_at).to be_within(15.minutes).of(8.days.ago)
    end
  end
end
