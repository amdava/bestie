# frozen_string_literal: true

require "rails_helper"
require "rake"

RSpec.describe "rake schedule_emails" do
  include ActiveJob::TestHelper

  it "runs" do
    Bestie::Application.load_tasks if Rake::Task.tasks.none? { |t| t.name == "schedule_emails" }
    expect { Rake::Task["schedule_emails"].invoke }.to change(enqueued_jobs, :size).by(2)
    job_classes = enqueued_jobs.map { |j| j["job_class"] }
    expect(job_classes).to include(SendPromptsJob.name)
    expect(job_classes).to include(SendNotOkAlertsJob.name)
  end
end
