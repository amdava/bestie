# frozen_string_literal: true

require "rails_helper"
require "rake"

RSpec.describe "rake db:seed" do
  it "runs" do
    Bestie::Application.load_tasks if Rake::Task.tasks.none? { |t| t.name == "db:seed" }
    ENV["verbose"] = "false"
    Rake::Task["db:seed"].invoke
  end
end
