require "rails_helper"

RSpec.describe FriendshipsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/friendships").to route_to("friendships#index")
    end

    it "routes to #destroy" do
      expect(delete: "/friendships/1").to route_to("friendships#destroy", id: "1")
    end
  end
end
