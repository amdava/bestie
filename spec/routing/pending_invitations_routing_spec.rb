# frozen_string_literal: true

require "rails_helper"

RSpec.describe PendingInvitationsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/pending_invitations").to route_to("pending_invitations#index")
    end

    it "routes to #update via PUT" do
      expect(put: "/pending_invitations/1").to route_to("pending_invitations#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/pending_invitations/1").to route_to("pending_invitations#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/pending_invitations/1").to route_to("pending_invitations#destroy", id: "1")
    end
  end
end
