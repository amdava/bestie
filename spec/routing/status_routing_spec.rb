# frozen_string_literal: true

require "rails_helper"

RSpec.describe StatusController, type: :routing do
  describe "routing" do
    it "routes to #create" do
      expect(get: "/status").to route_to("status#create")
    end
  end
end
