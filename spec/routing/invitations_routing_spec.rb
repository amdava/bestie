require "rails_helper"

RSpec.describe InvitationsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/invitations").to route_to("invitations#index")
    end

    it "routes to #create" do
      expect(post: "/invitations").to route_to("invitations#create")
    end

    it "routes to #destroy" do
      expect(delete: "/invitations/1").to route_to("invitations#destroy", id: "1")
    end
  end
end
