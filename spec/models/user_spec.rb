# frozen_string_literal: true

require "rails_helper"

RSpec.describe User, type: :model do
  context "with a user" do
    before { @user = create(:user) }

    it "is readable" do
      expect(@user.to_s).to include(@user.class.name)
      expect(@user.to_s).to include(@user.id)
      expect(@user.to_s).to include(@user.email)
    end

    it "has the defaults" do
      expect(@user.invites_left).to eq(2)
    end

    it "populate names" do
      @user.email = "name@example.com"
      @user.name = nil
      @user.nickname = nil
      @user.save!
      expect(@user.name).to eq("name")
      expect(@user.nickname).to eq("name")

      @user.name = "Name Last"
      @user.nickname = nil
      @user.save!
      expect(@user.name).to eq("Name Last")
      expect(@user.nickname).to eq("Name")

      @user.name = "Name Last"
      @user.nickname = "Name"
      @user.save!
      expect(@user.name).to eq("Name Last")
      expect(@user.nickname).to eq("Name")
    end

    it "has friendships" do
      create(:friendship, user: @user)
      create(:friendship, friend: @user)
      expect(@user.friendships.count).to eq(2)
      expect(@user.friends.count).to eq(2)
    end

    it "can be prompted" do
      expect(@user.latest_prompt_at).to be(nil)
      expect(@user.oldest_unanswered_prompt_at).to be(nil)
      expect(@user.log_in_token).to be(nil)

      Timecop.freeze(7.days.ago) { @user.generate_prompt_data! }

      expect(@user.latest_prompt_at).to be_within(15.minutes).of(7.days.ago)
      expect(@user.oldest_unanswered_prompt_at).to be_within(15.minutes).of(7.days.ago)
      expect(@user.log_in_token).not_to be(nil)

      Timecop.freeze(2.hours.ago) { @user.generate_prompt_data! }

      expect(@user.latest_prompt_at).to be_within(15.minutes).of(2.hours.ago)
      expect(@user.oldest_unanswered_prompt_at).to be_within(15.minutes).of(7.days.ago)
      expect(@user.log_in_token).not_to be(nil)
    end

    it "can update status" do
      Timecop.freeze(10.hours.ago) { @user.generate_prompt_data! }
      expect(@user.oldest_unanswered_prompt_at).not_to be(nil)

      Timecop.freeze(2.hours.ago) { @user.update_status! }

      expect(@user.latest_status_at).to be_within(15.minutes).of(2.hours.ago)
      expect(@user.oldest_unanswered_prompt_at).to be(nil)
    end

    it "is a fresh user" do
      expect(@user.ok?).to be(true)
      expect(@user.ok_but_recently_prompted?).to be(false)
      expect(@user.needs_prompt?).to be(false)
      expect(@user.needs_alerting_about?).to be(false)
      expect(@user.latest_prompt_at).to be(nil)
      expect(@user.oldest_unanswered_prompt_at).to be(nil)
      expect(@user.latest_status_at).to be_within(15.minutes).of(Time.zone.now)
      expect(@user.latest_not_ok_alert_at).to be(nil)

      Timecop.freeze(Time.zone.now + User::PROMPT_WINDOW) do
        expect(@user.needs_prompt?).to be(true)
      end
    end
  end

  it "has first prompt" do
    user = create(:user, :brad_pitt)

    expect(user.ok?).to be(true)
    expect(user.ok_but_recently_prompted?).to be(true)
    expect(user.needs_prompt?).to be(false)
    expect(user.needs_alerting_about?).to be(false)
    expect(user.latest_prompt_at).to be_within(15.minutes).of(2.hours.ago)
    expect(user.oldest_unanswered_prompt_at).to eq(user.latest_prompt_at)
    expect(user.latest_status_at).to be_within(15.minutes).of(10.hours.ago) # Hard-coded
    expect(user.latest_not_ok_alert_at).to be(nil) # Hard-coded.
  end

  it "has first prompt and is expired" do
    user = create(:user, :scarlett_johansson)

    expect(user.ok?).to be(false)
    expect(user.ok_but_recently_prompted?).to be(false)
    expect(user.needs_prompt?).to be(false)
    expect(user.needs_alerting_about?).to be(true)
    expect(user.latest_prompt_at).to be_within(15.minutes).of(30.hours.ago)
    expect(user.oldest_unanswered_prompt_at).to eq(user.latest_prompt_at)
    expect(user.latest_status_at).to be_within(15.minutes).of(40.hours.ago) # Hard-coded
    expect(user.latest_not_ok_alert_at).to be(nil) # Hard-coded.
  end

  it "has first prompt, is expired and alerted about" do
    user = create(:user, :leonardo_dicaprio)

    expect(user.ok?).to be(false)
    expect(user.ok_but_recently_prompted?).to be(false)
    expect(user.needs_prompt?).to be(false)
    expect(user.needs_alerting_about?).to be(false)
    expect(user.latest_prompt_at).to be_within(15.minutes).of(30.hours.ago)
    expect(user.oldest_unanswered_prompt_at).to eq(user.latest_prompt_at)
    expect(user.latest_status_at).to be_within(15.minutes).of(40.hours.ago) # Hard-coded
    expect(user.latest_not_ok_alert_at).to be_within(15.minutes).of(6.hours.ago)
  end

  it "has first prompt and first status" do
    user = create(:user, :cate_blanchett)

    expect(user.ok?).to be(true)
    expect(user.ok_but_recently_prompted?).to be(false)
    expect(user.needs_prompt?).to be(false)
    expect(user.needs_alerting_about?).to be(false)
    expect(user.latest_prompt_at).to be_within(15.minutes).of(30.hour.ago)
    expect(user.oldest_unanswered_prompt_at).to be(nil)
    expect(user.latest_status_at).to be_within(15.minutes).of(1.hour.ago)
    expect(user.latest_not_ok_alert_at).to be(nil) # Hard-coded
  end

  it "has first prompt, first status, ready for second cycle" do
    user = create(:user, :robert_de_niro)

    expect(user.ok?).to be(true)
    expect(user.ok_but_recently_prompted?).to be(false)
    expect(user.needs_prompt?).to be(true)
    expect(user.needs_alerting_about?).to be(false)
    expect(user.latest_prompt_at).to be_within(15.minutes).of(10.days.ago)
    expect(user.oldest_unanswered_prompt_at).to be(nil)
    expect(user.latest_status_at).to be_within(15.minutes).of(9.days.ago)
    expect(user.latest_not_ok_alert_at).to be(nil) # Hard-coded
  end

  it "is on second prompt, but ok" do
    user = create(:user, :gal_gadot)

    expect(user.ok?).to be(true)
    expect(user.ok_but_recently_prompted?).to be(true)
    expect(user.needs_prompt?).to be(false)
    expect(user.needs_alerting_about?).to be(false)
    expect(user.latest_prompt_at).to be_within(15.minutes).of(2.hours.ago)
    expect(user.oldest_unanswered_prompt_at).to be(user.latest_prompt_at)
    expect(user.latest_status_at).to be_within(15.minutes).of(9.days.ago)
    expect(user.latest_not_ok_alert_at).to be(nil) # Hard-coded
  end

  it "is on second prompt, not ok" do
    user = create(:user, :al_pacino)

    expect(user.ok?).to be(false)
    expect(user.ok_but_recently_prompted?).to be(false)
    expect(user.needs_prompt?).to be(false)
    expect(user.needs_alerting_about?).to be(true)
    expect(user.latest_prompt_at).to be_within(15.minutes).of(30.hours.ago)
    expect(user.oldest_unanswered_prompt_at).to be(user.latest_prompt_at)
    expect(user.latest_status_at).to be_within(15.minutes).of(9.days.ago)
    expect(user.latest_not_ok_alert_at).to be(nil) # Hard-coded
  end

  it "is on second status, ok" do
    user = create(:user, :salma_hayek)

    expect(user.ok?).to be(true)
    expect(user.ok_but_recently_prompted?).to be(false)
    expect(user.needs_prompt?).to be(false)
    expect(user.needs_alerting_about?).to be(false)
    expect(user.latest_prompt_at).to be_within(15.minutes).of(10.hours.ago)
    expect(user.oldest_unanswered_prompt_at).to be(nil)
    expect(user.latest_status_at).to be_within(15.minutes).of(1.hour.ago)
    expect(user.latest_not_ok_alert_at).to be(nil) # Hard-coded
  end

  it "has two prompts, neither answered" do
    user = create(:user, :harrison_ford)

    expect(user.ok?).to be(false)
    expect(user.ok_but_recently_prompted?).to be(false)
    expect(user.needs_prompt?).to be(false)
    expect(user.needs_alerting_about?).to be(true)
    expect(user.latest_prompt_at).to be_within(15.minutes).of(30.hours.ago)
    expect(user.oldest_unanswered_prompt_at).to be_within(15.minutes).of(10.days.ago)
    expect(user.latest_status_at).to be_within(15.minutes).of(11.days.ago) # Hard-coded
    expect(user.latest_not_ok_alert_at).to be(nil) # Hard-coded
  end

  it "has two prompts, neither answered, but was ok before" do
    user = create(:user, :milla_jovovich)

    expect(user.ok?).to be(false)
    expect(user.ok_but_recently_prompted?).to be(false)
    expect(user.needs_prompt?).to be(false)
    expect(user.needs_alerting_about?).to be(true)
    expect(user.latest_prompt_at).to be_within(15.minutes).of(30.hours.ago)
    expect(user.oldest_unanswered_prompt_at).to be_within(15.minutes).of(10.days.ago)
    expect(user.latest_status_at).to be_within(15.minutes).of(19.days.ago)
    expect(user.latest_not_ok_alert_at).to be(nil) # Hard-coded
  end
end

# == Schema Information
#
# Table name: users
#
#  id                          :uuid             not null, primary key
#  confirmation_sent_at        :datetime
#  confirmation_token          :string
#  confirmed_at                :datetime
#  current_sign_in_at          :datetime
#  current_sign_in_ip          :string
#  email                       :string           default(""), not null
#  encrypted_password          :string           default(""), not null
#  failed_attempts             :integer          default(0), not null
#  invites_left                :integer          default(0), not null
#  last_sign_in_at             :datetime
#  last_sign_in_ip             :string
#  latest_not_ok_alert_at      :datetime
#  latest_prompt_at            :datetime
#  latest_status_at            :datetime
#  locked_at                   :datetime
#  log_in_token                :string
#  name                        :string           not null
#  nickname                    :string           not null
#  oldest_unanswered_prompt_at :datetime
#  remember_created_at         :datetime
#  reset_password_sent_at      :datetime
#  reset_password_token        :string
#  sign_in_count               :integer          default(0), not null
#  unconfirmed_email           :string
#  unlock_token                :string
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
# Indexes
#
#  index_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_log_in_token          (log_in_token) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_unlock_token          (unlock_token) UNIQUE
#
