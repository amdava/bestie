# frozen_string_literal: true

require "rails_helper"

RSpec.describe Friendship, type: :model do
  before do
    @friendship = create(:friendship)
  end

  it "is readable" do
    expect(@friendship.to_s).to include(@friendship.class.name)
    expect(@friendship.to_s).to include(@friendship.id)
  end

  it "can't be connected to itself" do
    @friendship.user = @friendship.friend
    expect(@friendship).not_to be_valid
  end

  it "is double connected" do
    expect(Friendship.where(user: @friendship.friend, friend: @friendship.user)).to exist
  end

  it "deletes the inverse" do
    expect do
      @friendship.destroy
    end.to change(Friendship, :count).by(-2)
  end
end

# == Schema Information
#
# Table name: friendships
#
#  id            :uuid             not null, primary key
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  friend_id     :uuid             not null
#  invitation_id :uuid             not null
#  user_id       :uuid             not null
#
# Indexes
#
#  index_friendships_on_friend_id      (friend_id)
#  index_friendships_on_invitation_id  (invitation_id)
#  index_friendships_on_user_id        (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (friend_id => users.id)
#  fk_rails_...  (invitation_id => invitations.id)
#  fk_rails_...  (user_id => users.id)
#
