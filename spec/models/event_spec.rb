# frozen_string_literal: true

require "rails_helper"

RSpec.describe Event, type: :model do
  it "creates bare bones" do
    event = Event.create! name: :an_event

    event.reload
    expect(event.name).to eq("an_event")
    expect(event.rails_admin_label).to include("an_event")
  end

  it "creates full" do
    user1 = create(:user)
    user2 = create(:user)
    friendship = create(:friendship)

    event = Event.create! name: :an_event, from: user1, to: user2, record: friendship, data: {something: "Something"}

    event.reload
    expect(event.name).to eq("an_event")
    expect(event.from).to eq(user1)
    expect(event.to).to eq(user2)
    expect(event.record).to eq(friendship)
    expect(event.data).to eq({"something" => "Something"})
    expect(event.data["something"]).to eq("Something")
  end

  it "can't create without a name" do
    expect do
      Event.create!
    end.to raise_error
  end
end

# == Schema Information
#
# Table name: events
#
#  id          :uuid             not null, primary key
#  data        :json
#  name        :string           not null
#  record_type :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  from_id     :uuid
#  record_id   :uuid
#  to_id       :uuid
#
# Indexes
#
#  index_events_on_from_id  (from_id)
#  index_events_on_record   (record_type,record_id)
#  index_events_on_to_id    (to_id)
#
# Foreign Keys
#
#  fk_rails_...  (from_id => users.id)
#  fk_rails_...  (to_id => users.id)
#
