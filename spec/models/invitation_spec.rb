# frozen_string_literal: true

require "rails_helper"

RSpec.describe Invitation, type: :model do
  before do
    @invitation = create(:invitation)
  end

  it "is readable" do
    expect(@invitation.to_s).to include(@invitation.class.name)
    expect(@invitation.to_s).to include(@invitation.id)
    expect(@invitation.to_s).to include(@invitation.requester.email)
    expect(@invitation.to_s).to include(@invitation.email)
    expect(@invitation.rails_admin_label).to include(@invitation.requester.name)
    expect(@invitation.rails_admin_label).to include(@invitation.email)
  end

  it "doesn't change invited_at" do
    invited_at = @invitation.invited_at
    @invitation.save!
    expect(@invitation.invited_at).to eq(invited_at)
  end
end

# == Schema Information
#
# Table name: invitations
#
#  id            :uuid             not null, primary key
#  accepted_at   :datetime
#  email         :string           not null
#  email_sent_at :datetime
#  invited_at    :datetime         not null
#  name          :string           not null
#  rejected_at   :datetime
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  requester_id  :uuid             not null
#
# Indexes
#
#  index_invitations_on_requester_id            (requester_id)
#  index_invitations_on_requester_id_and_email  (requester_id,email) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (requester_id => users.id)
#
