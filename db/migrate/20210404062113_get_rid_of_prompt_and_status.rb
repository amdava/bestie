class GetRidOfPromptAndStatus < ActiveRecord::Migration[6.1]
  def change
    drop_table :prompts
    drop_table :statuses
  end
end
