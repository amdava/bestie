# frozen_string_literal: true

class AddFieldsToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :latest_not_ok_alert_at, :datetime
    add_column :users, :name, :string, null: false
    add_column :users, :nickname, :string, null: false
  end
end
