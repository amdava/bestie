class ImproveFrienhsipFields < ActiveRecord::Migration[6.1]
  def change
    rename_column :friendships, :user_a_id, :user_id
    rename_column :friendships, :user_b_id, :friend_id
  end
end
