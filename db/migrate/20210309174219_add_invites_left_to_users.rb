# frozen_string_literal: true

class AddInvitesLeftToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :invites_left, :integer, null: false, default: 0
  end
end
