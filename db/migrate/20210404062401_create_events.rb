# frozen_string_literal: true

class CreateEvents < ActiveRecord::Migration[6.1]
  def change
    create_table :events, id: :uuid do |t|
      t.string :name, null: false
      t.references :from, foreign_key: {to_table: :users}, type: :uuid, null: true
      t.references :to, foreign_key: {to_table: :users}, type: :uuid, null: true
      t.references :record, polymorphic: true, type: :uuid, null: true
      t.json :data, null: true

      t.timestamps
    end
  end
end
