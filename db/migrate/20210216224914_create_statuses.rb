# frozen_string_literal: true

class CreateStatuses < ActiveRecord::Migration[6.1]
  def change
    create_table :statuses, id: :uuid do |t|
      t.references :user, type: :uuid, null: false, foreign_key: true
      t.string :status, null: false
      t.string :source, null: false
      t.datetime :set_at, null: false
      t.timestamps
    end
  end
end
