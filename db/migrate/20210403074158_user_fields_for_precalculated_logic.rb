# frozen_string_literal: true

class UserFieldsForPrecalculatedLogic < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :latest_status_at, :datetime, null: true
    add_column :users, :latest_prompt_at, :datetime, null: true
    add_column :users, :oldest_unanswered_prompt_at, :datetime, null: true
    add_column :users, :log_in_token, :string, null: true
    add_index :users, :log_in_token, unique: true
  end
end
