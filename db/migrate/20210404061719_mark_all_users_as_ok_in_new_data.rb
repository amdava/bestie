class MarkAllUsersAsOkInNewData < ActiveRecord::Migration[6.1]
  def up
    print "Marking all users as done, the great reset..."
    User.find_each do |user|
      user.latest_prompt_at = Time.zone.now
      user.update_status!
      print "."
    end
    puts "done."
  end

  def down
    # Nothing to do.
  end
end
