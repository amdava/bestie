# frozen_string_literal: true

class CreateInvitations < ActiveRecord::Migration[6.1]
  def change
    create_table :invitations, id: :uuid do |t|
      t.references :requester, type: :uuid, null: false, foreign_key: {to_table: :users}
      t.string :name, null: false
      t.string :email, null: false
      t.datetime :invited_at, null: false
      t.datetime :email_sent_at
      t.datetime :accepted_at
      t.datetime :rejected_at
      t.timestamps
    end

    add_index :invitations, %i[requester_id email], unique: true
  end
end
