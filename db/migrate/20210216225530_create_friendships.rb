# frozen_string_literal: true

class CreateFriendships < ActiveRecord::Migration[6.1]
  def change
    create_table :friendships, id: :uuid do |t|
      t.references :user_a, type: :uuid, null: false, foreign_key: {to_table: :users}
      t.references :user_b, type: :uuid, null: false, foreign_key: {to_table: :users}
      t.references :invitation, type: :uuid, null: false, foreign_key: true
      t.timestamps
    end
  end
end
