# frozen_string_literal: true

class CreatePrompts < ActiveRecord::Migration[6.1]
  def change
    create_table :prompts, id: :uuid do |t|
      t.references :user, type: :uuid, null: false, foreign_key: true
      t.string :token, null: false, index: true
      t.datetime :prompted_at, null: false
      t.timestamps
    end
  end
end
